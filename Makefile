SRC = ./wasm/app.cpp ./src/Scene.cpp ./src/UiSdl.cpp
OPT = --bind --std=c++14 -O2 -Wall -Wextra -s WASM=1 -s USE_SDL=2 -s USE_SDL_GFX=2 -s DISABLE_DEPRECATED_FIND_EVENT_TARGET_BEHAVIOR=0 -I./src/

all:
	mkdir -p public
	em++ $(OPT) -o public/app.js $(SRC)
	cp wasm/index.html ./public/

run:
	python3 -m http.server --directory ./public

clean:
	rm -rf ./public

