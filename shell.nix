with import <nixpkgs> {};

emscriptenStdenv.mkDerivation {

  name = "wasm-sdl-test";

  src = ./.;

  buildInputs = [
    gnumake
    python3
    emscriptenPackages.zlib
  ];

}

